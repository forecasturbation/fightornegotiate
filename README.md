# Fight or Negotiate?
A turn-based multiplayer strategy game.

## LICENSE

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

A copy of the GNU Affero General Public License is located in the file LICENSE.AGPL.

## Structure

The game is intented to be played online. The primary interface is a webpage with the map (containing the current game state), a row of seven buttons beneath that, and finally a chat interface beneath that.

Players submit their orders by typing a message to the judge in the chat interface. The judge is represented by one of the seven buttons; players may communicated directly with other players using the remaining six buttons, which function as tabs for the chat interface.

Upon submission, every order is immediately parsed. The parser checks whether it is sound (whether it can be converted into the game's internal representation of an order) and whether it is valid (whether it is possible given the current game state). The game will not accept orders that do not pass these checks, and will immediately notify the user if they fail.

At the conclusion of the negotiation and order-writing phases, the chat logs are parsed for valid orders, which are then sent to the adjudicator, along with the current game state. The adjudicator determines whether orders succeed or fail, and return the new game state.

Immediately after evaluation, the retreat and disbanding phase begins. Players submit their orders to the judge, where they are checked for soundness and validity as in the negotiation and order writing phase. Once the orders are in or a brief period of time passes, orders are evaluated, a new game state is returned, and a new turn begins. This cycle repeats until the victory condition is met, or until draw by mutual agreement of the remaining players.

## TODO

* command parser (in progress)
* interface
* adjudicator 
